import React, { Component } from "react";
import { connect } from "react-redux";
import "./BaiTapBookingTicket.css";
import HangGhe from "./HangGhe";
import ThongTinDatGhe from "./ThongTinDatGhe";
class BookingTicket extends Component {
  renderHangGhe = () => {
    return this.props.dataGhe.map((hangGhe, index) => {
      return (
        <div key={index}>
          <HangGhe hangGhe={hangGhe} soHangGhe={index} />
        </div>
      );
    });
  };
  render() {
    return (
      <div
        className="bookingMovie"
        style={{
          position: "fixed",
          width: "100%",
          height: "100%",
          backgroundImage: "url('./img/bookingTicket/bgmovie.jpg')",
          backgroundSize: "100%",
        }}
      >
        <div
          style={{
            position: "fixed",
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(0,0,0,0.6)",
          }}
        >
          <div className="container-fluid">
            <div className="row">
              <div className="col-8 text-center">
                <h2 className="text-warning">ĐẶT VÉ XEM PHIM CYBERLEARN</h2>
                <div className="mt-2 text-light" style={{ fontSize: "25px" }}>
                  Màn hình
                </div>
                <div
                  className="mt-2"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                  }}
                >
                  <div className="screen ml-5"></div>
                  {this.renderHangGhe()}
                </div>
              </div>
              <div className="col-4">
                <h2 className="text-warning">DANH SÁCH GHẾ BẠN CHỌN</h2>
                <ThongTinDatGhe />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    dataGhe: state.datVeReducer.dataGhe,
  };
};

export default connect(mapStateToProps)(BookingTicket);
